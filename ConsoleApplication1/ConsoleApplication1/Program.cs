﻿using ConsoleApplication1.Metier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            GestionLogBI log = new GestionLogBI();
            log.EcrireLog("Début test 1");
            Console.WriteLine("Je test la gestion de fichier");
            log.EcrireLog("Fin du test 1");

            log.RecupDateFichierLogs();
        }
    }
}

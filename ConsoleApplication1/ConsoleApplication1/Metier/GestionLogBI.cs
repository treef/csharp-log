﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1.Metier
{
    class GestionLogBI
    {
        public string Ligne { get; set; }

        public string PathFichier { get; set; }

        public GestionLogBI()
        {
            this.Ligne = "";
            
            string date = DateTime.Now.ToString("ddMMyyyy");
            this.PathFichier = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" +  BData.NOM_FICHIER  + date + ".txt";
            CreerFichierLog();
           
        }

        public void CreerFichierLog()
        {
            if (!File.Exists(this.PathFichier))
            {
                var fs = File.Create(this.PathFichier);
                fs.Close();
                //Thread.Sleep(2000);
            }
        }

        public void EcrireLog(string message)
        {

            using (StreamWriter sw = new StreamWriter(this.PathFichier, append: true))
            {
                try
                {
                    //Ecrire une ligne dans le fichier 
                    this.Ligne = CreerLigneLog(message);
                    //sw.WriteLine(this.Ligne);
                    sw.WriteLine(this.Ligne);
                    //Fermeture flux DuplicateWaitObjectException fichier
                        
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        public string RecupererDate()
        {
            // Get the current date.
            return DateTime.Now.ToString();
        }

        public string CreerLigneLog(string message){

            return this.RecupererDate() + " - " + message; 
        }

        public void RecupDateFichierLogs()
        {
            string[] files = Directory.GetFiles(Environment.ExpandEnvironmentVariables("%USERPROFILE%") + @"\Documents");
            DateTime today = DateTime.Now;
            foreach (string file in files)
            {
                if(file.Contains(BData.NOM_FICHIER))
                {
                    int jour, mois, annee;
                    this.JourMoisAnnee(this.DateString(file) , out jour,out mois,out annee);
                    DateTime datetime = new DateTime(annee, mois, jour);
                    //bool verif = false;
                    var diff = today - datetime;
                    if(diff.Days > 14)
                    {
                        if(File.Exists(file))
                        {
                            File.Delete(file);
                        }
                    }
                }
            }
        }

        public void JourMoisAnnee(string date, out int jour, out int mois, out int annee)
        {
            jour = Convert.ToInt32(date.Substring(0, 2));
            mois = Convert.ToInt32(date.Substring(2, 2));
            annee = Convert.ToInt32(date.Substring(4));
        }

        public string DateString(string pathFichier)
        {
            string fichierSansChemin = Path.GetFileName(pathFichier);
            string dateString = fichierSansChemin.Replace(BData.NOM_FICHIER, "").Replace(".txt", "");

            return dateString;
        }
    }
}
